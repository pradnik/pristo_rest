<?xml version='1.0' encoding='utf-8'?>
<helpusingdocument xmlns='http://www.lotus.com/dxl' version='9.0' maintenanceversion='1.7'
 replicaid='C12581CC00278A0B' publicaccess='false' designerversion='8.5' default='true'>
<noteinfo noteid='122' unid='000001753D049D4685255BAC00564FC3' sequence='82'>
<created><datetime>19910212T104245,15-05</datetime></created>
<modified><datetime>20171102T081152,61+01</datetime></modified>
<revised><datetime dst='true'>20130919T143520,86-04</datetime></revised>
<lastaccessed><datetime>20171102T081152,61+01</datetime></lastaccessed>
<addedtofile><datetime>20171102T081152,61+01</datetime></addedtofile></noteinfo>
<updatedby><name>CN=Lotus Notes Template Development/O=Lotus Notes</name></updatedby>
<wassignedby><name>CN=Lotus Notes Template Development/O=Lotus Notes</name></wassignedby>
<body><richtext>
<pardef id='1' hide='notes web'/>
<par def='1'><run><font size='9pt' style='underline' color='blue'/></run><actionhotspot
 hotspotstyle='none'><code event='onClick' for='web'><javascript>history.back()</javascript></code><code
 event='onClick' for='client'><javascript>history.back()</javascript></code><run
><font size='9pt' style='underline' color='blue'/>Go back</run></actionhotspot><run
><font size='9pt' style='underline' color='blue'/></run></par>
<pardef id='2' spacebefore='1.5' hide='web'/>
<par def='2'><run><font size='9pt' style='underline' color='blue'/></run><actionhotspot
 hotspotstyle='none'><code event='click'><formula>@Command([FileCloseWindow])</formula></code><run
><font size='9pt' style='underline' color='blue'/>Close this document to view the database</run></actionhotspot><run
><font size='9pt' style='underline' color='blue'/></run></par>
<pardef id='3' align='center'/>
<par def='3'><run><font size='18pt' style='bold extrude' color='#400000'/>Using Discussion</run></par>
<pardef id='4'/>
<par def='4'><run><font size='11pt' style='bold'/></run></par>
<pardef id='5' leftmargin='1in' spacebefore='1.5'/>
<par def='5'><run><font size='9pt' style='bold'/>To follow a discussion</run></par>
<pardef id='6' leftmargin='1.2500in' spacebefore='1.5'/>
<par def='6'><run><font size='9pt'/>Select the All Documents view to see a chronological listing of main topics for discussion and their responses.  Select the By Author view to see what individual members of the discussion group have said.  Select the By Category view to browse the group's discussions categorized by main topics.</run></par>
<par def='4'><run><font size='9pt' style='bold'/></run></par>
<par def='4'><run><font size='9pt' style='bold'/>To begin a discussion</run></par>
<par def='6'><run><font size='9pt'/>From the All Documents view, By Author view or By Category view, create a Main Topic document to start a new topic for discussion.  Be sure to enter a category for the topic (Training, Documentation, Technical Tips, etc.).</run></par>
<pardef id='7' leftmargin='1in' firstlineleftmargin='1.2500in'/>
<par def='7'><run><font size='9pt'/></run></par>
<par def='4'><run><font size='9pt' style='bold'/>To respond to a main topic or a response document</run></par>
<par def='6'><run><font size='9pt' style='underline'/>Menu</run></par>
<par def='6'><run><font size='9pt'/>If you want to respond to a main topic, first highlight the main topic document and then select Create, Response.  Fill out the response form.</run>  <run
><font size='9pt'/>When you are done, press</run> <run><font size='9pt' style='bold'/>ESC</run> <run
><font size='9pt'/>and answer Yes to save your new document.</run></par>
<par def='6'><run><font size='9pt'/>If you want to respond to the message in a response document, highlight the appropriate response document and then select Create, Response to Response.  Fill out the response form and then save your changes.</run></par>
<par def='6'><run><font size='9pt' style='underline'/>Actions</run></par>
<par def='6'><run><font size='9pt'/>Open the document you wish to respond to and click the Response action button at the top of the document.  </run></par>
<par def='6'><run><font size='9pt' style='underline'/>Web Browser</run></par>
<par def='6'><run><font size='9pt'/>Open the document you wish to respond to and click on the New Response hotspot at the top of the document.  Fill out the response form and then click on either the Submit hotspot on the top of the response form or the Submit button at the bottom of the response form.  Response to Response documents are not supported on the Web.</run></par>
<par def='7'><run><font size='9pt'/></run></par>
<pardef id='8' leftmargin='0.7500in' firstlineleftmargin='1in'/>
<par def='8'><run><font size='9pt' style='bold'/>To create an Author Profile</run></par>
<par def='6'><run><font size='9pt'/>If you would like people to know something about you, click the "New Author Profile" button at the top of the view.  Fill out the form and add an (optional) picture of yourself.</run></par>
<par def='8'><run><font size='9pt'/></run></par>
<par def='8'><run><font size='9pt' style='bold'/>To flag a document as Private</run></par>
<pardef id='9' leftmargin='1.2500in' firstlineleftmargin='1.2896in' spacebefore='1.5'/>
<par def='9'><run><font size='9pt'/>You can use the "Mark Private" and</run><run
><font size='9pt' style='bold'/> </run><run><font size='9pt'/>"Mark Public" actions in the template to control whether anyone other than yourself can read a specific document.  For example, if you have not completed the writing of a particular document, you can click the "Mark Private" action and others will not be able to see the document.  When you complete the document, you can click the</run><run
><font size='9pt' style='bold'/> </run><run><font size='9pt'/>"Mark Public"</run><run
><font size='9pt' style='bold'/> </run><run><font size='9pt'/>action to make it available for others to read.</run></par>
<pardef id='10' leftmargin='1in' firstlineleftmargin='1.2500in' spacebefore='1.5'/>
<par def='10'><run><font size='9pt'/>Note:  This action is not available to Anonymous web users.</run></par>
<par def='8'><run><font size='9pt'/></run></par>
<par def='8'><run><font size='9pt' style='bold'/>To set up an Interest Profile</run></par>
<par def='6'><run><font size='9pt' style='underline'/>Notes</run></par>
<par def='6'><run><font size='9pt'/>Use the Action menu item Edit Interest Profile to bring up your personal interest profile document. You can elect to be notified via a newsletter if certain conditions are met. These conditions can include the appearance of your name or phrases that you designate.</run></par>
<par def='6'><run><font size='9pt' style='underline'/>Web Browser</run></par>
<par def='6'><run><font size='9pt'/>Select the Edit Profile hotspot from any of the views in the database to bring up your personal interest profile document. You can elect to be notified via a newsletter if certain conditions are met. These conditions can include the appearance of your name or phrases that you designate.  This feature is not available to Anonymous web users.</run></par>
<par def='7'><run><font size='9pt'/></run></par>
<par def='8'><run><font size='9pt' style='bold'/>To add the current discussion topic to your Interest Profile</run></par>
<par def='6'><run><font size='9pt' style='underline'/>Notes</run></par>
<par def='6'><run><font size='9pt'/>Use the Action menu item Add Selected Topic To Interest Profile  to add the current discussion topic to the list of items you wish to track via newsletter.</run></par>
<par def='6'><run><font size='9pt' style='underline'/>Web Users</run></par>
<par def='6'><run><font size='9pt'/>Open the Main Topic which you would like to be added to your interest profile and select the Add Topic to Interest Profile hotspot.  This action is available whether you are reading or editing a main topic document.  This feature is not available to Anonymous web users.</run></par>
<par def='7'><run><font size='9pt' style='bold'/></run></par>
<par def='8'><run><font size='9pt' style='bold'/>Anonymous responses</run></par>
<par def='6'><run><font size='9pt'/>If you wish to respond anonymously to a discussion, highlight the main topic or a response and select Create, Other.  Then select either Anonymous Response or Anonymous Response to Response.  This feature is not available to web users.  If an anonymous web user is able to open the discussion database,  all documents created on the web will reflect that the user is anonymous.</run></par>
<par def='8'><run><font size='9pt' style='bold'/></run></par>
<par def='8'><run><font size='9pt' style='bold'/>Agents</run></par>
<par def='6'><run><font size='9pt' style='underline'/>Newsletters</run></par>
<par def='6'><run><font size='9pt'/>A user can elect to be notified via e-mail when a new response has been added to certain topic(s) of discussion.  These topics are specified in the user's Interest Profile.  There are seven agents pertaining to newsletters.</run></par>
<pardef id='11' leftmargin='1.5000in' spacebefore='1.5'/>
<par def='11'><run><font size='9pt' color='teal'/>Edit Interest Profile:  </run><run
><font size='9pt'/>The Interest Profile contains individual topics, phrases, keywords or categories of interest to the individual.  It can be tailored to contain as wide or as narrow a selection of topics as the individual is seeking information on.  This document is used by the </run><run
><font size='9pt' style='italic'/>Send Newsletters</run><run><font size='9pt'/> and </run><run
><font size='9pt' style='italic'/>Add Selected Topic to Interest Profile</run><run
><font size='9pt'/> agents, described below.</run></par>
<par def='11'><run><font size='9pt' color='teal'/>WebEditInterest Profile:  </run><run
><font size='9pt'/>Web users equivalent of the Edit Interest Profile agent.</run></par>
<par def='11'><run><font size='9pt' color='teal'/>Web InterestProfileSave:  </run><run
><font size='9pt'/>Saves the Interest Profiles for web users.</run></par>
<par def='11'><run><font size='9pt' color='teal'/>Add Selected Topic to Interest Profile: </run><run
><font size='9pt'/> Allows the user to add new topics of interest to their individual interest profiles.  After a new topic has been added, a Notes agent will process this request and notify the user via e-mail whenever new responses to this topic are added to the database.  If the error "This is not identified as a thread. Contact the database manager if you want all threads initialized." appears, the database manager must run the agent called </run><run
><font size='9pt' style='italic'/>Initialize ThreadIds</run><run><font size='9pt'/> (see description below).</run></par>
<par def='11'><run><font size='9pt' color='teal'/>WebAddTopic:  </run><run
><font size='9pt'/>Web users equivalent of Add Selected Topic to Interest Profile agent.</run></par>
<par def='11'><run><font size='9pt' color='teal'/>Send  Newsletters:</run><run
><font size='9pt'/>  Reviews the "Interest Profile" of each user who has a profile on a server-based Discussion database.  It matches criteria on the profile with any criteria it finds in the topics of the current database.  Each match generates a document link, which will become part of the newsletter; that newsletter is then mailed to the subscriber.  The agent can be run periodically.  Typically this would be on a daily basis.  </run></par>
<par def='11'><run><font size='9pt' color='teal'/>WebRemoveThread: </run><run
><font size='9pt'/> Removes the selected thread from the web users Interest Profile.  </run></par>
<par def='11'><run><font size='9pt' color='teal'/>Update Thread Maps:  </run><run
><font size='9pt'/>Enables thread mapping for main topics and responses for web users.  Thread mapping displays a list of all topics and responses in the current thread on the document that the user is currently viewing.</run></par>
<pardef id='12' leftmargin='1.2500in'/>
<par def='12'><run><font size='9pt'/></run></par>
<pardef id='13' leftmargin='1in'/>
<par def='13'><run><font size='9pt' style='bold'/>Archiving</run></par>
<par def='6'><run><font size='9pt' color='teal'/>Notes users only: </run><run
><font size='9pt' style='italic'/> </run><run><font size='9pt'/>To set up Archiving for the Discussion, click the Archive button on the second tab of the File-Database-Properties infobox.  Fill out the information and click the OK button.  Set up the Archiving task to run on the server.  An archive database will be set up automatically.</run></par>
<par def='6'><run><font size='9pt' style='underline'/>Marking documents as "Expired"  </run></par>
<par def='6'><run><font size='9pt'/>The Archive feature can act upon documents that have been marked as Expired.  </run></par>
<par def='6'><run><font size='9pt' color='teal'/>Notes users:  </run><run
><font size='9pt'/>Select a document in the view and choose Mark/Unmark Expired from the Actions menu.</run></par>
<par def='6'><run><font size='9pt' color='teal'/>Web browser users:  </run><run
><font size='9pt'/>Put the document into edit mode by clicking the Edit Document hotspot.  Then click the Mark/Unmark Expired hotspot at the top of the document.  </run></par>
<par def='12'><run><font size='9pt'/></run></par>
<par def='13'><run><font size='9pt' style='bold'/>Subscriptions</run></par>
<par def='6'><run><font size='9pt'/>This template has a customized subscription form, containing the same fields as are found in the Interest Profile.  Select Create-Subscription to create a new subscription in your Headline database.  </run></par>
<par def='6'><run><font size='9pt' color='teal'/>Converting Interest Profiles to Subscriptions:</run><run
><font size='9pt'/>  If you have an existing interest profile, you can convert its data into a subscription.  Select Actions-Convert Interest Profile to Subscription.  Note that this action will convert the data and then delete your existing interest profile.  It can only be used by Notes clients running R5.0 or higher.  Subscriptions are not available for the web at this time.</run></par>
<par def='12'><run><font size='9pt'/></run></par>
<par def='13'><run><font size='9pt' style='bold'/>Deleting Documents on the Web</run></par>
<par def='6'><run><font size='9pt'/>You can now delete documents on the web while at the view level.  Click the document once to 'highlight' it and click the "Move to Trash" button.  A trash can icon will appear next to the document.  To remove the document permanently from the database, click the "Empty Trash" button.</run></par>
<par def='12'><run><font size='9pt'/></run></par>
<par def='13'><run><font size='9pt' style='bold'/>Attaching Files via the Web Browser</run></par>
<par def='6'><run><font size='9pt'/>Only one file can be attached to a specific document at a time from a web browser.  However, multiple files may be attached by attaching the file, saving and closing the document, and then reopening the document to attach the next file.  This process can be repeated as many times as necessary to attach multiple files.</run></par>
<par def='4'><run><font size='9pt'/></run></par>
<pardef id='14' hide='notes'/>
<par def='14'><run><font size='9pt' name='Arial' pitch='variable' truetype='true'
 familyid='20' color='teal'/></run><button width='2in' widthtype='maximum'
 maxlines='1' wraptext='true' bgcolor='system'><code event='onClick' for='web'><javascript
>window.close(self)</javascript></code><code event='onClick' for='client'><javascript
>window.close(self)</javascript></code><font size='9pt'/>Done</button><run
><font size='9pt'/></run></par></richtext></body>
<item name='OriginalModTime' sign='true'><datetime dst='true'>20050415T100122,54-07</datetime></item></helpusingdocument>

