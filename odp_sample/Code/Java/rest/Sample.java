package rest;

import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lotus.domino.NotesException;
import lotus.domino.Session;

import bean.AppBean;

import com.ibm.xsp.extlib.util.ExtLibUtil;

@Path("/sample")
@Tag(name = "samples")
public class Sample {

	@GET
	@Path("/hello/{greeting}")
	@Operation(summary = "Hello world sample operation", tags = {"messages"}, responses = { @ApiResponse(description = "sample message"),
			@ApiResponse(responseCode = "404", description = "should not happen")})
	public Response hello(@PathParam("greeting") final String greeting){
		Session s=ExtLibUtil.getCurrentSession();
		String message="";
		try {
			message = "<b>Hello " + s.getEffectiveUserName() + "</b><br/>" + greeting + "<br/>";
		} catch (NotesException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
		return Response.ok().type(MediaType.TEXT_HTML).entity(message).build();
	}
	
	@GET
	@Path("/counter")
	public Response count(){
		Map<String,Integer> map=new HashMap<String,Integer>();
		AppBean abean=(AppBean) resolveVariable("appBean");
		map.put("counter", abean.getCount());
		map.put("next", abean.getCount());
		return Response.ok(map, MediaType.APPLICATION_JSON).build();
	}
	
	private static Object resolveVariable(String variable) {
		try {
			return FacesContext.getCurrentInstance().getApplication()
					.getVariableResolver().resolveVariable(
							FacesContext.getCurrentInstance(), variable);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
