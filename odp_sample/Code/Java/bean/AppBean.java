package bean;

import java.io.Serializable;

public class AppBean implements Serializable {

	private static final long serialVersionUID = -1281894279933694275L;
	private Integer count=0;
	
	public Integer getCount(){
		return count++;
	}

}
