package to.pris.rest;
import java.io.IOException;

import javax.faces.FacesException;
import javax.faces.FactoryFinder;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.event.PhaseListener;
import javax.faces.lifecycle.Lifecycle;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.commons.util.NotImplementedException;
import com.ibm.domino.services.AbstractRestServlet;
import com.ibm.domino.xsp.module.nsf.NotesContext;
import com.ibm.xsp.application.ApplicationEx;
import com.ibm.xsp.context.FacesContextEx;
import com.ibm.xsp.controller.FacesController;
import com.ibm.xsp.controller.FacesControllerFactoryImpl;

import io.swagger.v3.jaxrs2.integration.JaxrsOpenApiContextBuilder;
import io.swagger.v3.oas.integration.OpenApiConfigurationException;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;

public class CustomWinkServlet extends AbstractRestServlet {
	
	private static Lifecycle dummyLifeCycle = new Lifecycle() {
        @Override
        public void render(FacesContext context) throws FacesException {
            throw new NotImplementedException();
        }
        @Override
        public void removePhaseListener(PhaseListener listener) {
            throw new NotImplementedException();
        }
        @Override
        public PhaseListener[] getPhaseListeners() {
            throw new NotImplementedException();
        }
        @Override
        public void execute(FacesContext context) throws FacesException {
            throw new NotImplementedException();
        }
        @Override
        public void addPhaseListener(PhaseListener listener) {
            throw new NotImplementedException();
        }
	};

	private static final long serialVersionUID = 1L;
	private ServletConfig config;
	private FacesContextFactory contextFactory;
	private Boolean initialized=false;
	
	@Override
	protected void doDestroy() {
		System.out.println("Custom Wink Servlet destroyed...");
		super.doDestroy();
	}

	@Override
	protected void doInit() throws ServletException {
		System.out.println("Custom Wink Servlet initialized.");
	}
	
	@Override
	public void init(final ServletConfig config) throws ServletException {
		this.config = config;
		contextFactory = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
		super.init(config);
	}

	@Override
	public void doService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		NotesContext nc=NotesContext.getCurrentUnchecked();
    	String javaClassValue="plugin.Activator";
		String str = "WEB-INF/classes/" + javaClassValue.replace('.', '/') + ".class";
		nc.setSignerSessionRights(str);
		if (!initialized){ //initialization has do be done after NotesContext is initialized with session to support SessionAsSigner operations
			super.doInit();
		}
		FacesContext fc=null;
		try {
			fc = initContext(request, response);
	    	FacesContextEx exc = (FacesContextEx)fc;
	    	ApplicationEx ape = exc.getApplicationEx();
	    	if (ape.getController() == null) {
	    		FacesController controller = new FacesControllerFactoryImpl().createFacesController(getServletContext());
	    		controller.init(null);
	    	}
	    	
			super.doService(request, response);
		} catch(Throwable t) {
			t.printStackTrace();
			response.sendError(500, "Application failed!");
		} finally {
			if (fc != null) {
				releaseContext(fc);
			}
			
		}
	}
	
	@SuppressWarnings("unused")
	public FacesContext initContext(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Create a temporary FacesContext and make it available
		FacesContext context = contextFactory.getFacesContext(getServletConfig().getServletContext(), request, response,
				dummyLifeCycle);

		// custom server for OpenAPI
		OpenAPI oas = new OpenAPI();
		Server server = new Server();
		server.setUrl(request.getContextPath() + APIServletFactory.SERVLET_PATH);
		server.setDescription("Sample API Endpoint");
		oas.addServersItem(server);
		SwaggerConfiguration oasConfig = new SwaggerConfiguration().openAPI(oas).prettyPrint(true);

		try {
			OpenAPI openAPI = new JaxrsOpenApiContextBuilder().openApiConfiguration(oasConfig).buildContext(true)
					.read();
		} catch (OpenApiConfigurationException e) {
			throw new RuntimeException(e.getMessage(), e);
		}

		return context;
	}
	
	@SuppressWarnings("deprecation")
	public void releaseContext(FacesContext context) throws ServletException, IOException {
    			context.release();
    }
	
	@Override
    public ServletConfig getServletConfig() {
    	return config;
    }
	
	@Override
	 protected ClassLoader getContextClassLoader() {
        return NotesContext.getCurrentUnchecked().getModule().getModuleClassLoader();
    }

}
