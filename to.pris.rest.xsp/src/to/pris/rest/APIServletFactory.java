package to.pris.rest;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import com.ibm.designer.runtime.domino.adapter.ComponentModule;
import com.ibm.designer.runtime.domino.adapter.IServletFactory;
import com.ibm.designer.runtime.domino.adapter.ServletMatch;
import com.ibm.domino.xsp.module.nsf.NotesContext;

public class APIServletFactory implements IServletFactory {

	private ComponentModule module;
	public static final String SERVLET_PATH = "/xsp/.jaxrs/";
	private Servlet servlet;
	private long lastUpdate;

	@Override
	public ServletMatch getServletMatch(String contextPath, String path) throws ServletException {

		if (path.startsWith(SERVLET_PATH)) { // $NON-NLS-1$
			int len = SERVLET_PATH.length(); // $NON-NLS-1$
			String servletPath = path.substring(0, len);
			String pathInfo = path.substring(len);
			return new ServletMatch(getExecutorServlet(), servletPath, pathInfo);
		}
		return null;
	}

	@Override
	public void init(ComponentModule module) {
		this.module = module;
		this.lastUpdate = module.getLastRefresh();
	}

	public synchronized Servlet getExecutorServlet() throws ServletException {
		if (servlet == null || lastUpdate < this.module.getLastRefresh()) {
			Map<String,String> params=new HashMap<String,String>();
			params.put("applicationConfigLocation", "/wink_application");
			params.put("propertiesLocation", "/wink.properties");
			
			servlet = module.createServlet(new CustomWinkServlet(), "Wink Servlet", params);
			lastUpdate = this.module.getLastRefresh();
		}
		return servlet;
	}
}

