package to.pris.rest.xsp;

import com.ibm.xsp.library.AbstractXspLibrary;

import to.pris.rest.Activator;

public class XSPLibrary extends AbstractXspLibrary {
	
	public final static String LIBRARY_ID = XSPLibrary.class.getName();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.xsp.library.XspLibrary#getLibraryId()
	 */
	@Override
	public String getLibraryId() {
		return LIBRARY_ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.xsp.library.AbstractXspLibrary#getPluginId()
	 */
	@Override
	public String getPluginId() {
		return Activator.PLUGIN_ID;
	}

	@Override
	public String[] getFacesConfigFiles() {
		// TODO Auto-generated method stub
		return super.getFacesConfigFiles();
	}

	@Override
	public String[] getXspConfigFiles() {
		// TODO Auto-generated method stub
		return super.getXspConfigFiles();
	}

}