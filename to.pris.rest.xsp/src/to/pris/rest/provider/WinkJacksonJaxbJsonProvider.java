package to.pris.rest.provider;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

import javax.ws.rs.core.MediaType;

@Provider
@Consumes( {MediaType.APPLICATION_JSON, "text/json"})
@Produces( {MediaType.APPLICATION_JSON, "text/json"})
public class WinkJacksonJaxbJsonProvider extends JacksonJaxbJsonProvider implements MessageBodyReader<Object>, MessageBodyWriter<Object> {

    public WinkJacksonJaxbJsonProvider() {
        super(createObjectMapper(), BASIC_ANNOTATIONS);
    }

    private static ObjectMapper createObjectMapper() {
    	ObjectMapper objMapper = new ObjectMapper();
		
		// register AnnotationProcessors: Jackson & JAXB
		AnnotationIntrospector primary = new JacksonAnnotationIntrospector();
		AnnotationIntrospector secondary = new JaxbAnnotationIntrospector(objMapper.getTypeFactory());
		AnnotationIntrospector pair = AnnotationIntrospector.pair(primary, secondary);
		
		objMapper.getDeserializationConfig().withInsertedAnnotationIntrospector(pair);
		objMapper.getSerializationConfig().withInsertedAnnotationIntrospector(pair);
				
		// globally disable empty values
		objMapper.setSerializationInclusion(Include.NON_EMPTY);
        return objMapper;
    }

}

